import os
import sys


def remove_pair(nom, carte):
    nom_enc = os.popen(f"echo {nom} | openssl enc -aes-128-cbc -pbkdf2 -a -k ./RAM/key -nosalt").read().replace('\n', '')
    carte_enc = os.popen(f"echo {carte} | openssl enc -aes-128-cbc -pbkdf2 -a -k ./RAM/key -nosalt").read().replace('\n', '')
    old = open("disque/data", "r")

    lines = old.readlines()
    old.close()

    new = open("disque/data", "w")
    for line in lines:
        if line.strip("\n") != f"{nom_enc},{carte_enc}":
            new.write(line)
    new.close()


if __name__ == '__main__':
    remove_pair(sys.argv[1], sys.argv[2])