import base64
import os
import secrets
import shutil


def ask_for_mdp():
    return input("Entrez votre mot de passe : \n")


def change_to_be_hex(s):
    return int(s, base=16)


def init():
    if os.path.exists('./RAM'):
        shutil.rmtree("RAM")
    if os.path.exists('./disque'):
        shutil.rmtree("disque")
    if os.path.exists('./usb1'):
        shutil.rmtree("usb1")
    if os.path.exists('./usb2'):
        shutil.rmtree("usb2")
    if os.path.exists('./usb3'):
        shutil.rmtree("usb3")
    if os.path.exists('./usb4'):
        shutil.rmtree("usb4")

    os.mkdir("RAM")
    os.mkdir("disque")
    os.mkdir("usb1")
    os.mkdir("usb2")
    os.mkdir("usb3")
    os.mkdir("usb4")

    # Génère quatres clef aléatoire nécessaires pour les clefs chiffrées
    big_token = secrets.token_hex(8)
    token1 = secrets.token_hex(8)
    token2 = secrets.token_hex(8)
    token3 = secrets.token_hex(8)
    token4 = secrets.token_hex(8)

    #
    mdp1 = ask_for_mdp()
    mdp2 = ask_for_mdp()
    mdp3 = ask_for_mdp()
    mdp4 = ask_for_mdp()

    os.system(f"echo {token1} | openssl enc -aes-256-cbc -k {mdp1} -out usb1/key ")
    os.system(f"echo {token1} | openssl enc -aes-256-cbc -k {mdp2} -out usb2/key")
    os.system(f"echo {token1} | openssl enc -aes-256-cbc -k {mdp3} -out usb3/key")
    os.system(f"echo {token1} | openssl enc -aes-256-cbc -k {mdp4} -out usb4/key")

    # XOR binaire pour "fusionner" les clefs
    x13 = change_to_be_hex(token1) ^ change_to_be_hex(token3)
    x14 = change_to_be_hex(token1) ^ change_to_be_hex(token4)
    x23 = change_to_be_hex(token2) ^ change_to_be_hex(token3)
    x24 = change_to_be_hex(token2) ^ change_to_be_hex(token4)

    # Convertion en base64
    x13b64 = base64.b64encode(str(x13).encode('ascii')).decode('utf-8')
    x14b64 = base64.b64encode(str(x14).encode('ascii')).decode('utf-8')
    x23b64 = base64.b64encode(str(x23).encode('ascii')).decode('utf-8')
    x24b64 = base64.b64encode(str(x24).encode('ascii')).decode('utf-8')

    combinedb64 = base64.b64encode(str(big_token).encode('ascii')).decode('utf-8')

    os.system(f"echo {big_token} | openssl enc -aes-256-cbc -out disque/x13b64 -k {x13b64} 2>/dev/null")
    os.system(f"echo {big_token} | openssl enc -aes-256-cbc -out disque/x14b64 -k {x14b64} 2>/dev/null")
    os.system(f"echo {big_token} | openssl enc -aes-256-cbc -out disque/x23b64 -k {x23b64} 2>/dev/null")
    os.system(f"echo {big_token} | openssl enc -aes-256-cbc -out disque/x24b64 -k {x24b64} 2>/dev/null")


if __name__ == '__main__':
    init()
