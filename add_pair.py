import os
import sys
from subprocess import PIPE, Popen


def add_pair(nom, carte):
    f = open("disque/data", 'a+')
    nom_enc = os.popen(f"echo {nom} | openssl enc -aes-128-cbc -pbkdf2 -a -k ./RAM/key -nosalt").read().replace('\n', '')
    carte_enc = os.popen(f"echo {carte} | openssl enc -aes-128-cbc -pbkdf2 -a -k ./RAM/key -nosalt").read().replace('\n', '')
    f.write(f"{nom_enc},{carte_enc}\n")


if __name__ == '__main__':
    add_pair(sys.argv[1], sys.argv[2])
