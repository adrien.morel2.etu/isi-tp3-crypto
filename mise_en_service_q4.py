import secrets
import os
import hashlib
import base64


def ask_for_mdp():
    return input("Entrez votre mot de passe : \n")


def change_to_be_hex(s):
    return int(s, base=16)


def init():
    is_resp1 = input("Etes vous resp?(1) (y/n) \n")
    mdp1 = ask_for_mdp()
    is_resp2 = input("Etes vous resp?(2) (y/n) \n")
    mdp2 = ask_for_mdp()

    if is_resp1 == 'y':
        token1 = os.popen(f"openssl enc -aes-256-cbc -d -k '{mdp1}' -a -in usb1/key 2>/dev/null -iv 0 -nosalt")
        a = 1
    else:
        token1 = os.popen(f"openssl enc -aes-256-cbc -d -k '{mdp1}' -a -in usb2/key 2>/dev/null -iv 0 -nosalt")
        a = 2

    if is_resp2 == 'y':
        token2 = os.popen(f"openssl enc -aes-256-cbc -d -k '{mdp2}' -a -in usb3/key 2>/dev/null -iv 0 -nosalt")
        b = 3
    else:
        token2 = os.popen(f"openssl enc -aes-256-cbc -d -k '{mdp2}' -a -in usb4/key 2>/dev/null -iv 0 -nosalt")
        b = 4

    combined_key = change_to_be_hex(token1) ^ change_to_be_hex(token2)

    os.system(f"openssl enc -aes-256-cbc -d -k '{combined_key}' -in disque/x{a}{b}64 -out RAM/key -iv 0 -nosalt")


if __name__ == '__main__':
    init()
