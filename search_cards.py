import os
import sys


def search_cards(nom):
    nom_enc = os.popen(f"echo {nom} | openssl enc -aes-128-cbc -pbkdf2 -a -k ./RAM/key -nosalt").read().replace('\n', '')
    old = open("disque/data", "r")

    lines = old.readlines()
    old.close()

    cards = []
    for line in lines:
        if line.startswith(nom_enc):
            cards.append(line.split(',')[1])
    return cards


if __name__ == '__main__':
    print(search_cards(sys.argv[1]))
