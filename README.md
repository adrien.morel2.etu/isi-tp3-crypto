# ISI TP3 Crypto

Hassan Oualid , Morel Adrien

## Question 1

- On génère aléatoirement deux tokens.
- Le premier utilisateur entre son mdp(1).
- Le deuxième utilisateur entre son mdp(2).
- Token 1 est chiffré sur la clef usb1 par le mdp1
- Token 2 est chiffré sur la clef usb2 par le mdp2
- La clef de déchiffrage sera le XOR des deux token (sur usb1 et usb2).
Celle-ci sera stockée sur la ram de façon à ne pas pouvoir être exploitée par une faille de sécurité physique
  (la ram ne conserve ses données que si elle est allimentée donc impossible
 de la retirée pour en lire les données)

## Question 2

Voir fichiers init_et_mise_en_service.py, add_pair.py, remove_pair.py et search_cards.py

## Question 3 

On créer une clé pour chaque responsable/représentant, et pour chaque clé, on créer une combinaisons avec une autre clé 
qui permet de déchiffrer la clé globale. Avant la mise en service, on demande à l'utilsateur si c'est un représentant ou
responsable afin de pouvoir retrouver la bonne combinaison.

## Question 4

Voir fichier init_q4.py et mise_en_service_q4.py

## Question 5 

Pour répudier une personne du système il faut supprimer toutes les combinaisons dont il fait partis dans le dossier disque.
Pour cela on stocke toutes les combinaisons possibles des tokens de 3 personnes, si elles sont rassemblées et que leurs clefs 
déchifrées le permettent alors on répudie cette personne comme expliqué précédemment.
