import secrets
import shutil
import os
import hashlib
import base64


def ask_for_mdp():
    return input("Entrez votre mot de passe : \n")


def change_to_be_hex(s):
    return int(s, base=16)


def init():
    if os.path.exists('./RAM'):
        shutil.rmtree("RAM")
    if os.path.exists('./disque'):
        shutil.rmtree("disque")
    if os.path.exists('./usb1'):
        shutil.rmtree("usb1")
    if os.path.exists('./usb2'):
        shutil.rmtree("usb2")
    if os.path.exists('./usb3'):
        shutil.rmtree("usb3")
    if os.path.exists('./usb4'):
        shutil.rmtree("usb4")

    os.mkdir("RAM")
    os.mkdir("disque")
    os.mkdir("usb1")
    os.mkdir("usb2")

    # Génère deux clef aléatoire nécessaires pour les clefs chiffrées des usb1 et usb2
    clef_un = secrets.token_hex(8)
    clef_deux = secrets.token_hex(8)

    # Calcule la somme de controle (checksum) des mdps
    sha_mdp_un = hashlib.sha256(ask_for_mdp().encode('utf-8')).hexdigest()
    sha_mdp_deux = hashlib.sha256(ask_for_mdp().encode('utf-8')).hexdigest()

    # Chiffre les données aléatoires avec les mdp hashé des utilisateurs afin d'obtenir des clefs sur usb1 et usb2
    os.system(f"echo '{clef_un}' | openssl enc -aes-128-cbc -pbkdf2 -K {sha_mdp_un} -out ./usb1/clef.enc -iv 0 -nosalt")
    os.system(f"echo '{clef_deux}' | openssl enc -aes-128-cbc -pbkdf2 -k {sha_mdp_deux} -out ./usb2/clef.enc -iv 0 -nosalt")

    # XOR sur la paire de données aléatoires afin d'obtenir le clef de chiffrement du fichier banquaire
    xored = change_to_be_hex(clef_un) ^ change_to_be_hex(clef_deux)
    # Convertion en base64
    based = base64.b64encode(str(xored).encode('ascii')).decode('utf-8')

    f = open("RAM/key", "w+")
    f.write(based)
    f.close()

    # Chiffre tous les fichiers contenu dans 'disque'
    #for filename in os.listdir('disque'):
        #os.system(f"openssl enc -aes-128-cbc -pbkdf2 -k {based} -in {filename} -out {filename}.enc")


if __name__ == '__main__':
    init()
